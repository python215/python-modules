"""
Simple Math module
"""

"""
Addition of two integers
"""
def Add(in1, in2):
    return in1+in2

"""
Subtraction  of two integers
"""
def Sub(in1, in2):
    return in1-in2

"""
Multiplication of two integers
"""
def Mul(in1, in2):
    return in1*in2

"""
AND of two integers 
"""
def And(in1, in2):
    return in1&in2

"""
OR of two integers 
"""
def Or(in1, in2):
    return in1|in2

"""
XOR of two integers
"""
def Xor(in1, in2):
    return in1^in2

"""
XNOR of two integers
"""
def XNor(in1, in2):
    return ~(in1^in2)
