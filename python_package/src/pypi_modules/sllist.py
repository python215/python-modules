class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

    def getData(self):
        return self.data

    def setData(self, data):
        self.data = data

class LinkedList:
    def __init__(self):
        self.head = None

    def insertNode(self, newNode):
        if self.head is None:
            self.head = newNode
        else:
            tempNode = self.head
            while tempNode.next is not None:
                tempNode = tempNode.next
            tempNode.next = newNode

    def insertHeadNode(self, newNode):
        tempNode = self.head
        self.head = newNode
        self.head.next = tempNode

    def lengthOfList(self):
        if self.head is None:
            return (0, [])
        count = 0
        list_ = []
        tempNode = self.head
        while tempNode is not None:
            count += 1
            list_.append(tempNode.data)
            tempNode = tempNode.next
        return (count, list_)

    def insertNodeBefore(self, newNode, pos):
        count, _ = self.lengthOfList()
        if pos < 1 and pos > count:
            print(f"Invalid position {pos} to insert Node!")
        elif pos == 1:
            self.insertHeadNode(newNode)
        else:
            currentNode = self.getNodeAt(pos)
            previousNode = self.getNodeAt(pos-1)
            previousNode.next = newNode
            newNode.next = currentNode

    def insertNodeAfter(self, newNode, pos):
        count, _ = self.lengthOfList()
        if pos < 1 and pos > count:
            print(f"Invalid position {pos} to insert Node!")
        else:
            currentNode = self.getNodeAt(pos)
            nextNode = self.getNodeAt(pos+1)
            currentNode.next = newNode
            newNode.next = nextNode

    def getNodeAt(self, pos):
        count, _ = self.lengthOfList()
        if pos == 0 or pos > count:
            print(f"Node at pos {pos} is None.")
            return None
        if pos == 1:
            return self.head

        tempNode = self.head
        curPos = 1
        while True:
            if curPos is pos:
                return tempNode
            tempNode = tempNode.next
            curPos += 1

    def getPrevNode(self, newNode):
        if newNode is self.head:
            return None

        currentNode = self.head
        while True:
            if currentNode.next is newNode:
                return currentNode
            currentNode = currentNode.next
            if currentNode.next is None:
                return None

        return None

    def getNextNode(self, newNode):
        currentNode = self.head
        while True:
            nextNode = currentNode.next
            if currentNode is newNode:
                return nextNode
            currentNode = currentNode.next

            if currentNode.next is None:
                return None
        return None

    def swapNodes(self, firstNode, secondNode):
        prevfirstNode = self.getPrevNode(firstNode) # 20
        prevsecondNode = self.getPrevNode(secondNode) # 30
        nextsecondNode = self.getNextNode(secondNode) # 50

        if prevfirstNode is not None:
            prevfirstNode.next = secondNode # 20=>40
        if prevsecondNode is not None:
            prevsecondNode.next = firstNode

        secondNode.next = firstNode.next
        firstNode.next = nextsecondNode

        if prevfirstNode is None:
            self.head = secondNode
        elif prevsecondNode is None:
            self.head = firstNode
        """
        prevfirstNode.next = secondNode # 20=>40
        prevsecondNode.next = firstNode

        temp = firstNode.next
        firstNode.next = secondNode.next
        secondNode.next = temp
        """

    def swapWithPos(self, pos1, pos2):
        node1 = self.getNodeAt(pos1)
        node2 = self.getNodeAt(pos2)
        if node1 is None or node2 is None:
            print("No Operations has been Done, As One of Node at position is None.")
            return None
        self.swapNodes(node1, node2)
        return True

    def displayList(self):
        tempNode = self.head
        while tempNode is not None:
            print(tempNode.data, end='=>')
            tempNode = tempNode.next
        print("None")

    def deleteHead(self):
        tempNode = self.head
        self.head = self.head.next
        del tempNode

    def deleteNodeAt(self, pos):
        count, _ = self.lengthOfList()
        if pos == 0:
            self.deleteHead()
            #return True
        elif pos < 0 or pos >= count:
            print("Node Not Fount in list!.")
            #return False
        else:
            index = 0
            tempNode = self.head
            while index != pos:
                prevNode = tempNode
                tempNode = tempNode.next
                index += 1
            prevNode.next = tempNode.next
            del tempNode

    def sortList(self):
        currentpos = 1
        count, _ = self.lengthOfList()
        while currentpos != count:
            innerLoopIndex = 1
            while innerLoopIndex != count:
                node1 = self.getNodeAt(innerLoopIndex)
                #print(f"Node1 data: {node1.data}")
                node2 = self.getNodeAt(innerLoopIndex+1)
                #print(f"Node2 data: {node2.data}")
                if node1.data > node2.data:
                    self.swapNodes(node1, node2)
                innerLoopIndex += 1
            currentpos += 1

    def reverseList(self):
        currentpos = 0
        count, _ = self.lengthOfList()
        while currentpos != count-1:
            innerLoopIndex = 1
            while innerLoopIndex != count-currentpos:
                node1 = self.getNodeAt(innerLoopIndex)
                #print(f"Node1 data: {node1.data}")
                node2 = self.getNodeAt(innerLoopIndex+1)
                #print(f"Node2 data: {node2.data}")
                self.swapNodes(node1, node2)
                innerLoopIndex += 1
            currentpos += 1

    def deleteNode(self, delNode):
        if delNode.data == self.head.data:
            self.deleteHead()
            return False
        currentNode = self.head
        prevNode = self.head
        while True:
            if currentNode.data == delNode.data:
                tempNode = currentNode.next
                del currentNode
                prevNode.next = tempNode
                break
            prevNode = currentNode
            currentNode = currentNode.next
        return True

    def removeDuplicates(self):
        node1 = self.head
        while node1 is not None and node1.next is not None:
            node2 = node1
            while node2.next is not None:
                if node1.data == node2.next.data:
                    temp = node2.next
                    node2.next = node2.next.next
                    del temp
                    #self.deleteNode(temp) # wrong
                else:
                    node2 = node2.next
            node1 = node1.next

"""
        currentpos = 0
        while currentpos != self.lengthOfList():
            innerLoopIndex = 1
            while innerLoopIndex != self.lengthOfList():
                node1 = self.getNodeAt(innerLoopIndex)
                #print(f"Node1 data: {node1.data}")
                node2 = self.getNodeAt(innerLoopIndex+1)
                #print(f"Node2 data: {node2.data}")
                if node1.data == node2.data:
                    self.deleteNodeAt(innerLoopIndex+1)
                else:
                    self.swapNodes(node1, node2)
                innerLoopIndex += 1
            currentpos += 1
"""

