"""
Simple String module
"""

"""
Length of a string
"""
def Len(str1):
    return len(str1)

"""
Concat of a string
"""
def Concat(str1, str2):
    return str1+str2

"""
Equal or not
"""
def isEq(str1, str2):
    return str1 == str2
    # return str1 is str2

"""
Reverse of a string
"""
def Rev(str1):
    return str1[::-1]

"""
check substring in string
"""
def Exist(str1, str2):
    return str1 in str2

"""
Check if a Substring is Present in a Given String
"""
def Find(str1, str2):
    return str1.find(str2)
    # return str1.count(str2)

"""
Returns UPPER of a Given String
"""
def ToUpper(str1):
    return str1.upper()

"""
Returns LOWER of a Given String
"""
def ToLower(str1):
    return str1.lower()





