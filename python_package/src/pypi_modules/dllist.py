class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

    @classmethod
    def getDataFromNode(cls):
        #return self.data
        return True

    @classmethod
    def setDataOfNode(cls, data):
        # self.data = data
        return data

class LinkedList:
    def __init__(self):
        self.head = None

    def insertNode(self, newNode):
        if self.head is None:
            self.head = newNode
        else:
            tempNode = self.head
            while True:
                if tempNode.next is None:
                    tempNode.next = newNode
                    newNode.prev = tempNode
                    break
                tempNode = tempNode.next

    def insertHeadNode(self, newNode):
        if self.head is None:
            self.head = newNode
        else:
            tempNode = self.head
            self.head = newNode
            newNode.next = tempNode
            tempNode.prev = newNode

    def insertNodeAfter(self, index, newNode):
        currentNode = self.getNodeAt(index)
        if currentNode is not None and currentNode.next is not None:
            nextNode = currentNode.next
            currentNode.next = newNode
            newNode.next = nextNode
            newNode.prev = currentNode
            nextNode.prev = newNode
        elif currentNode.next is None:
            # could be last node
            self.insertNode(newNode)

    def insertNodeBefore(self, index, newNode):
        if index < 0 or index > self.lengthOfList():
            print("Invalid Index.")
            return False
        if index == 0:
            self.insertHeadNode(newNode)
            return True
        self.insertNodeAfter(index-1, newNode)
        return True

    def getNodePosition(self, node):
        currentIndex = 0
        currentNode = self.head

        while currentNode is not None and currentNode.data != node.data:
            currentIndex += 1
            currentNode = currentNode.next
        if currentNode is None:
            print("Node Not found!")
        else:
            print(f"Node Found at index: {currentIndex}")
        return currentIndex

    def getNodeAt(self, index):
        if index < 0 and index >= self.lengthOfList():
            print("Invalid Index received!")
            return None
        if index == 0:
            return self.head
        currentIndex = 0
        currentNode = self.head
        while currentNode is not None:
            currentNode = currentNode.next
            currentIndex += 1
            if currentIndex == index:
                return currentNode

    def getNodeDataAt(self, index):
        node = self.getNodeAt(index)
        if node is None:
            return None
        return node.data

    def deleteHeadNode(self):
        tempNode = self.head
        self.head = tempNode.next
        del tempNode

    def deleteNode(self, inputNode):
        if self.head is inputNode:
            self.deleteHeadNode()
            return
        currentNode = self.head
        previousNode = self.head
        while currentNode is not None:
            if currentNode.data == inputNode.data:
                if currentNode.next is not None:
                    nextNode = currentNode.next
                    previousNode.next = nextNode
                    nextNode.prev = previousNode
                elif currentNode.next is None:
                    previousNode.next = None

                del currentNode
                break

            previousNode = currentNode
            currentNode = currentNode.next

    def deleteNodeAt(self, index):
        node = self.getNodeAt(index)
        if node is None:
            print(f"Invalid index: {index}.")
            return
        self.deleteNode(node)
        return

    """
    If node1 and node2 are non-NULL members of a doubly-linked list (circular or not), swap their data fields.
    """
    @classmethod
    def swap_data(cls, node1, node2):
        temp_data = node1.data
        node1.data = node2.data
        node2.data = temp_data

    def swapNodesWithoutData(self, node1, node2):
        if node1 == node2:
            print("Can't Swap two same nodes!")

        previousNode1 = None
        currentNode1 = self.head
        while currentNode1 is not None and currentNode1.data != node1.data:
            previousNode1 = currentNode1
            currentNode1 = currentNode1.next

        previousNode2 = None
        currentNode2 = self.head
        while currentNode2 is not None and currentNode2.data != node2.data:
            previousNode2 = currentNode2
            currentNode2 = currentNode2.next

        if currentNode1 is None or currentNode2 is None:
            print("One or either nodes are None.")

        if previousNode1 is not None:
            previousNode1.next = currentNode2
        else:
            self.head = currentNode2

        if previousNode2 is not None:
            previousNode2.next = currentNode1
        else:
            self.head = currentNode1

        tempNode1 = currentNode1.next
        currentNode1.next = currentNode2.next
        currentNode2.next = tempNode1

    def swapNodesWithData(self, node1, node2):
        if node1 is not None and node2 is not None:
            self.swap_data(node1, node2)

    def swapWithPos(self, index1, index2):
        node1 = self.getNodeAt(index1)
        node2 = self.getNodeAt(index2)
        print(f"SwapNodes of data {node1.data}: {node2.data}")
        if node1 is None or node2 is None:
            print("No Operations has been Done, As One of Node at position is None.")
            return False
        #self.swapNodesWithData(node1, node2)
        self.swapNodesWithoutData(node1, node2)
        return True

    def sortList(self):
        currentpos = 0
        while currentpos != self.lengthOfList()-1:
            innerLoopIndex = 0
            while innerLoopIndex != self.lengthOfList()-1:
                node1 = self.getNodeAt(innerLoopIndex)
                node2 = self.getNodeAt(innerLoopIndex+1)
                if node1.data > node2.data:
                    self.swapNodesWithoutData(node1, node2)
                    self.displayList()
                    self.displayListReverse()
                innerLoopIndex += 1
            currentpos += 1

    def lengthOfList(self):
        if self.head is None:
            return 0
        count = 0
        tempNode = self.head
        while tempNode is not None:
            count += 1
            tempNode = tempNode.next
        return count

    def displayList(self):
        if self.head is None:
            print("List is Empty!")
            return
        tempNode = self.head
        while tempNode is not None:
            print(tempNode.data, end='=>')
            tempNode = tempNode.next
        print("None")

    def getList(self):
        returnList = []
        if self.head is None:
            return returnList
        tempNode = self.head
        while tempNode is not None:
            returnList.append(tempNode.data)
            tempNode = tempNode.next
        return returnList

    def displayListReverse(self):
        tempNode = self.head
        while tempNode.next is not None:
            tempNode = tempNode.next
        #print(f"Last node: {tempNode.data}")
        while tempNode is not None:
            print(tempNode.data, end="=>")
            tempNode = tempNode.prev
        print("None")

    def displayPreviousNodes(self):
        if self.head is None:
            print("List is Empty!")
            return
        tempNode = self.head
        while tempNode is not None:
            #print(tempNode.data, end='=>')
            if tempNode.prev is None:
                print("None", end="=>")
            else:
                print(tempNode.prev.data, end='=>')
            tempNode = tempNode.next
        print("None")

