## How to install
```pip install -i https://test.pypi.org/simple/ pypi-modules==0.1.1```

## Example usage 
```
from pypi_modules import simplemath 
print(simplemath.simplemath_Add(2,3)) 
```
## Roadmap
* Enhancing linked lists modules
* Support for publishing package to PYPI for both windows & Linux
* More Python modules 
* Enhancing version using git
* Adding release candidate in CI/CD pipeline
