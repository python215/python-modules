import sys
import os
import setuptools
from setuptools import setup, find_packages

PACKAGE_NAME = 'pypi_modules'
VERSION_FILENAME = 'version.py'
DESCRIPTION = 'Package for working with Simple python modules'
AUTHOR = 'GP RAVADA'
AUTHOR_EMAIL = 'gp.ravada@gmail.com'

VERSION_PATH = os.path.join(os.path.dirname(__file__), "src", PACKAGE_NAME, VERSION_FILENAME)

# Require pytest-runner only when running tests
pytest_runner = (['pytest-runner>=2.0,<3dev']
                 if any(arg in sys.argv for arg in ('pytest', 'test'))
                 else [])

setup_requires = pytest_runner
main_ns = {}
with open(VERSION_PATH) as ver_file:
    exec(ver_file.read(), main_ns)
VERSION = main_ns['__version__']

# name="simple_pypi_package_gravad",

setup(
    name=PACKAGE_NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
    long_description_content_type = 'text/markdown',    
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    license="MIT",
    include_package_data=True,
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    tests_require=['pytest'],
    setup_requires=setup_requires
)
