import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src/pypi_modules')

import simplemath


def test_Add():
    assert simplemath.Add(1,2) == 3
    print("test_Add---PASS")

def test_Sub():
    assert simplemath.Sub(2,1) == 1
    print('test_Sub---PASS')

def test_Mul():
    assert simplemath.Mul(1,2) == 2
    print('test_Mul---PASS')

def test_And():
    assert simplemath.And(1,2) == (1&2)
    print('test_And---PASS')

def test_Or():
    assert simplemath.Or(1,2) == (1|2)
    print('test_Or-----PASS')

def test_Xor():
    assert simplemath.Xor(1,2) == (1^2)
    print('test_Xor---PASS')

def test_XNor():
    assert simplemath.XNor(1,2) == ~(1^2)
    print('test_XNor-PASS')
