import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src/pypi_modules')

from sllist import Node, LinkedList

class NewNode(Node):
    def __init__(self, data):
        super().__init__(data)
        self.isRead = False

def test_LengthofList():
    n1 = NewNode("10")
    n2 = NewNode("20")
    n3 = NewNode("30")
    l_list = LinkedList()
    l_list.insertNode(n1)
    l_list.insertNode(n2)
    l_list.insertNode(n3)
    (length, l) = l_list.lengthOfList()
    assert length == 3
    assert l == ["10", "20", "30"]

def test_HeadNode():
    n1 = NewNode("10")
    n2 = NewNode("20")
    n3 = NewNode("30")
    l_list = LinkedList()
    l_list.insertNode(n1)
    l_list.insertNode(n2)
    l_list.insertNode(n3)
    nHead = l_list.getNodeAt(0)
    assert nHead == None
    nHead = l_list.getNodeAt(1)
    assert nHead.data == "10"   
    n4 = NewNode("5")
    l_list.insertHeadNode(n4)               
    nHead = l_list.getNodeAt(1)
    assert nHead.data == "5"
    (length, l) = l_list.lengthOfList()
    assert length == 4 
    assert l == ["5", "10", "20", "30"]

def test_GetNode():
    n1 = NewNode("10")
    n2 = NewNode("20")
    n3 = NewNode("30")
    l_list = LinkedList()
    l_list.insertNode(n1)
    l_list.insertNode(n2)
    l_list.insertNode(n3)
    nHead = l_list.getNodeAt(0)
    assert nHead == None
    n = l_list.getNodeAt(1)
    assert n.data == "10"
    n = l_list.getNodeAt(2)
    assert n.data == "20"
    n = l_list.getNodeAt(3)
    assert n.data == "30"
    n = l_list.getPrevNode(n1)
    assert n == None
    n = l_list.getPrevNode(n2)
    assert n == n1
    n = l_list.getPrevNode(n3)
    assert n == n2
    n = l_list.getNextNode(n1)
    assert n == n2        
    n = l_list.getNextNode(n2)
    assert n == n3        

def test_ReverseList():
    n1 = NewNode("10")
    n2 = NewNode("20")
    n3 = NewNode("30")
    l_list = LinkedList()
    l_list.insertNode(n1)
    l_list.insertNode(n2)
    l_list.insertNode(n3)
    l_list.reverseList()
    (length, l) = l_list.lengthOfList()
    assert length == 3
    assert l == ["30", "20", "10"]

def test_swapNodes():
    n1 = NewNode("10")
    n2 = NewNode("20")
    n3 = NewNode("30")
    l_list = LinkedList()
    l_list.insertNode(n1)
    l_list.insertNode(n2)
    l_list.insertNode(n3)
    (length, l) = l_list.lengthOfList()
    assert length == 3
    assert l == ["10", "20", "30"]
    l_list.swapNodes(n1,n2)
    (length, l) = l_list.lengthOfList()
    assert length == 3
    assert l == ["20", "10", "30"]

def test_DeleteNode():
    n1 = NewNode("10")
    n2 = NewNode("20")
    n3 = NewNode("30")
    l_list = LinkedList()
    l_list.insertNode(n1)
    l_list.insertNode(n2)
    l_list.insertNode(n3)
    (length, l) = l_list.lengthOfList()
    assert length == 3
    assert l == ["10", "20", "30"]
    l_list.deleteHead() 
    (length, l) = l_list.lengthOfList()
    assert length == 2
    assert l == ["20", "30"]
    l_list.deleteHead() 
    (length, l) = l_list.lengthOfList()
    assert length == 1
    assert l == ["30"]
    l_list.deleteHead() 
    (length, l) = l_list.lengthOfList()
    assert length == 0
    assert l == []

def test_removeDuplicates():
    n1 = NewNode("10")
    n2 = NewNode("20")
    n3 = NewNode("30")
    l_list = LinkedList()
    l_list.insertNode(n1)
    l_list.insertNode(n2)
    l_list.insertNode(n3)
    (length, l) = l_list.lengthOfList()
    assert length == 3
    assert l == ["10", "20", "30"]
    dNode = NewNode("10")
    l_list.insertNode(dNode)
    (length, l) = l_list.lengthOfList()
    assert length == 4
    assert l == ["10", "20", "30", "10"]
    l_list.removeDuplicates()
    (length, l) = l_list.lengthOfList()
    assert length == 3
    assert l == ["10", "20", "30"]
    
