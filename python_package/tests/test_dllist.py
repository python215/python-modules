import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src/pypi_modules')

from dllist import Node, LinkedList

class NewNode(Node):
    def __init__(self, data):
        super().__init__(data)
        self.isRead = False

def test_LengthofList():
    ten = NewNode("10")
    twenty = NewNode("20")
    thirty = NewNode("30")
    forty = NewNode("40")
    fifty = NewNode("50")
    dll = LinkedList()
    length= dll.lengthOfList()
    assert length == 0
    dll.insertNode(ten)
    dll.insertNode(twenty)
    dll.insertNode(thirty)
    dll.insertNode(forty)
    dll.insertNode(fifty)
    length= dll.lengthOfList()
    assert length == 5

def test_getList():
    ten = NewNode("10")
    twenty = NewNode("20")
    thirty = NewNode("30")
    forty = NewNode("40")
    fifty = NewNode("50")
    dll = LinkedList()
    length= dll.lengthOfList()
    assert length == 0
    dll.insertNode(ten)
    dll.insertNode(twenty)
    dll.insertNode(thirty)
    dll.insertNode(forty)
    dll.insertNode(fifty)
    length= dll.lengthOfList()
    assert length == 5
    dlist = dll.getList()
    assert len(dlist) == 5
    assert dlist == ["10", "20", "30", "40", "50"]

def test_getNodeIndex():
    ten = NewNode("10")
    twenty = NewNode("20")
    thirty = NewNode("30")
    forty = NewNode("40")
    fifty = NewNode("50")
    dll = LinkedList()
    length= dll.lengthOfList()
    assert length == 0
    dll.insertNode(ten)
    dll.insertNode(twenty)
    dll.insertNode(thirty)
    dll.insertNode(forty)
    dll.insertNode(fifty)
    length= dll.lengthOfList()
    assert length == 5
    pos = dll.getNodePosition(ten)    
    assert pos == 0
    pos = dll.getNodePosition(forty)    
    assert pos == 3
    pos = dll.getNodePosition(fifty)    
    assert pos == 4    
    node = dll.getNodeAt(0)
    assert node == ten
    node = dll.getNodeAt(3)
    assert node == forty
    node = dll.getNodeAt(4)
    assert node == fifty    
    node = dll.getNodeAt(5)
    assert node == None
    nodeData = dll.getNodeDataAt(0)      
    nodeData == "10"
    nodeData = dll.getNodeDataAt(4)      
    nodeData == "50"    

def test_swapNodes():
    ten = NewNode("10")
    twenty = NewNode("20")
    thirty = NewNode("30")
    forty = NewNode("40")
    fifty = NewNode("50")
    dll = LinkedList()
    length= dll.lengthOfList()
    assert length == 0
    dll.insertNode(ten)
    dll.insertNode(twenty)
    dll.insertNode(thirty)
    dll.insertNode(forty)
    dll.insertNode(fifty)
    length= dll.lengthOfList()
    assert length == 5
    dlist = dll.getList()
    assert len(dlist) == 5
    assert dlist == ["10", "20", "30", "40", "50"]
    dll.swapWithPos(0,1)
    dlist = dll.getList()
    assert len(dlist) == 5
    assert dlist == ["20", "10", "30", "40", "50"]
