import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src/pypi_modules')

import simplestring


def test_Len():
    assert simplestring.Len("Hello") == 5
    print("test_Len---PASS")

def test_Concat():
    assert simplestring.Concat("Hello", " World!") == "Hello World!"
    print("test_Concat---PASS")

def test_isEq():
    assert simplestring.isEq("Hello World!", "Hello World!") == True
    assert simplestring.isEq("Hello World!", "Hello World") == False
    print("test_isEq---PASS")

def test_Rev():
    assert simplestring.Rev("Hello World!") == "!dlroW olleH"
    print("test_Rev---PASS")

def test_Exist():
    assert simplestring.Exist(" Wor", "Hello World!") == True
    assert simplestring.Exist(" Worfd", "Hello World!") == False
    print("test_Exist---PASS")

def test_Find():
    assert simplestring.Find("Hello World!", "llo") == 2
    print("test_Find---PASS")

def test_ToUpper():
    assert simplestring.ToUpper("Hello") == "HELLO"
    print("test_ToUpper---PASS")

def test_ToLower():
    assert simplestring.ToLower("Hello") == "hello"
    print("test_ToLower---PASS")
